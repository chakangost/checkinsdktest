package com.zeroweb.prefs;

import android.content.Context;
import android.content.SharedPreferences;

public class DevicePrefs {
    private static DevicePrefs instance = null;
    SharedPreferences pref = null;

    private DevicePrefs(Context context) {
        this.pref = context.getSharedPreferences("pref", context.MODE_PRIVATE);
    }

    public static DevicePrefs getInstance(Context context) {
        if (instance == null)
            instance = new DevicePrefs(context);
        return instance;
    }

    public String getDeviceUuid() {
        return pref.getString("deviceUuid", "");
    }

    public void setDeviceUuid(String deviceUuid) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("deviceUuid", deviceUuid);
        editor.apply();
    }

    public String getLastResult() {
        return pref.getString("lastResult", "");
    }

    public void setLastResult(String lastResult) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("lastResult", lastResult);
        editor.apply();
    }

    public String getAdid() {
        return pref.getString("advertisementId", "");
    }

    public void setAdId(String adId) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("advertisementId", adId);
        editor.apply();
    }

    public String getApiKey() {
        return pref.getString("apiKey", "");
    }

    public void setApiKey(String adId) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("apiKey", adId);
        editor.apply();
    }
}