package com.zeroweb.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.zeroweb.rsi.receiver.AlarmOneMinuteBroadcaseReceiver;
import com.zeroweb.rsi.util.AlarmUtils;
import com.zeroweb.util.Utility;

public class BootCompletedReceiver extends BroadcastReceiver {
    private Utility utility = Utility.getInstance();

    @Override
    public void onReceive(Context context, Intent intent) {
        // 구버전인 경우, 기존의 리시버에 전달한다

        //utility.scheduleJob(context);
    }
}
