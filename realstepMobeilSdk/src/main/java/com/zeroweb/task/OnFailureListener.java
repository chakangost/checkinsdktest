package com.zeroweb.task;

/**
 * Created by USER on 2017-09-01.
 */

public interface OnFailureListener<TResult> {
    void onFailure(Exception exception);
}