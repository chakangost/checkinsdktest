package com.zeroweb.task;

/**
 * Created by USER on 2017-09-01.
 */

public interface OnCompleteListener<TResult> {
    void onComplete(TResult task);
}
