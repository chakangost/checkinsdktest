package com.zeroweb.task;

/**
 * Created by USER on 2017-09-01.
 */

public class RealstepTask<TResult> {
    private final Object resultTx = new Object();
    private final Object exceptionTx = new Object();
    private final Object listenerTx = new Object();
    private OnCompleteListener<TResult> onCompleteListener = null;
    private OnSuccessListener<TResult> onSuccessListener = null;
    private OnFailureListener<TResult> onFailureListener = null;
    private boolean success = false;
    private boolean complete = false;
    private TResult result = null;
    private Exception exception = null;

    public RealstepTask<TResult> addOnCompleteListener(OnCompleteListener<TResult> onCompleteListener) {
        synchronized (listenerTx) {
            this.onCompleteListener = onCompleteListener;
        }
        return this;
    }

    public RealstepTask<TResult> addOnSuccessListener(OnSuccessListener<TResult> onSuccessListener) {
        synchronized (listenerTx) {
            this.onSuccessListener = onSuccessListener;
        }
        return this;
    }

    public RealstepTask<TResult> addOnFailureListener(OnFailureListener<TResult> onFailureListener) {
        synchronized (listenerTx) {
            this.onFailureListener = onFailureListener;
        }
        return this;
    }

    private boolean isResultNull() {
        synchronized (resultTx) {
            return this.result == null;
        }
    }

    public TResult getResult() {
        for ( int i = 0; isResultNull() && i <= 60; i++ ) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
            }
        }

        synchronized (resultTx) {
            return this.result;
        }
    }

    public void setResult(TResult result) {
        synchronized (resultTx) {
            this.result = result;
            success = true;
            complete = true;
            if (onCompleteListener != null)
                onCompleteListener.onComplete(this.result);
            if (onSuccessListener != null)
                onSuccessListener.onSuccess(this.result);
        }
    }

    public Exception getException() {
        synchronized (exceptionTx) {
            return this.exception;
        }
    }

    public void setException(Exception exception) {
        synchronized (exceptionTx) {
            success = false;
            complete = true;
            if (onCompleteListener != null)
                onCompleteListener.onComplete(null);
            if (onFailureListener != null)
                onFailureListener.onFailure(exception);
        }
    }
}
