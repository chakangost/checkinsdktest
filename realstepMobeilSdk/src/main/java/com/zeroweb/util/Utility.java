package com.zeroweb.util;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.zeroweb.rsi.util.AlarmUtils;
import com.zeroweb.service.RealstepPlaceJobService;

public class Utility {
    private static Utility instance = null;
    private AlarmUtils alarmUtils = AlarmUtils.getInstance();
    private Utility() {}

    public static Utility getInstance() {
        if (instance == null)
            instance = new Utility();
        return instance;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void scheduleJob(Context context) {
        Log.d("com.zeroweb.checkin", "위치 확인을 위해 스캔을 수행합니다");
        //ComponentName serviceComponent = new ComponentName(context, )
        ComponentName serviceComponent = new ComponentName(context, RealstepPlaceJobService.class);
        //create and schedule a jobinfo builder
        JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
        builder.setMinimumLatency(alarmUtils.getLoopTime()); // waiting time
        builder.setOverrideDeadline(alarmUtils.getLoopTime()); // max delay
        // device idle true
        //builder.setRequiresDeviceIdle(true);
        // works even on charging and discharging
        //builder.setRequiresCharging(false);

        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        if (jobScheduler == null) {
            // Log.d("+HGKIM", "@@@ jobScheduler is null");
            return;
        }

        jobScheduler.schedule(builder.build());
    }
}
