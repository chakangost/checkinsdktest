package com.zeroweb.auth;

/**
 * Created by USER on 2017-09-01.
 */

public class GetUserResult {
    private String deviceId;

    public GetUserResult(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceId() {
        return this.deviceId;
    }
}
