package com.zeroweb.auth;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.zeroweb.rsi.Setting;
import com.zeroweb.rsi.remote.RemoteIntentService;
import com.zeroweb.rsi.service.SpotService;
import com.zeroweb.rsi.support.Utils;
import com.zeroweb.rsi.util.EncryptionSHA512;
import com.zeroweb.rsi.vo.DeviceData;
import com.zeroweb.rsi.vo.enums.OsType;
import com.zeroweb.task.RealstepTask;

/**
 * Created by USER on 2017-08-31.
 */

public class RealstepUser extends RemoteIntentService {
    private static final String RSI_TAG = "rsi service : ";
    private static String mApiKey = "";
    private static String mServerVersion;

    public RealstepUser() {
        super("com.zeroweb.auth.RealstepUser");
        mServerVersion = Setting.getInstance().getVersion();
    }

    private void getApiKey() {
        try {
            PackageManager pm = getPackageManager();
            if (pm == null)
                return;
            ApplicationInfo ai = pm.getApplicationInfo(this.getPackageName(), PackageManager.GET_META_DATA);
            if (ai == null)
                return;
            Bundle bundle = ai.metaData;
            if (bundle == null)
                return;
            mApiKey = bundle.getString("com.zeroweb.rsi.key");
        } catch (PackageManager.NameNotFoundException e) {
        }
    }

    public RealstepTask<GetUserResult> getUser() {
        final RealstepTask<GetUserResult> rt = new RealstepTask<GetUserResult>();
        new Thread() {
            public void run() {
                GetUserResult gur = getCurrentUser();
//                Log.d("+swpark", "run: gur " + gur);
//                Log.d("+swpark", "run: gur.getDeviceId() " + gur.getDeviceId());
                if ( gur == null || gur.getDeviceId() == null )
                    rt.setException(new Exception("device id 를 가져올 수 없습니다."));
                else
                    rt.setResult(gur);
            }
        }.start();

        return rt;
    }

    private GetUserResult getCurrentUser() {
        String device_id = "";
        if ( Setting.getInstance().isRegistDeviceId() ) {
            device_id = Build.SERIAL;
        }

        DeviceData.DeviceRequest req = new DeviceData.DeviceRequest();
        req.setApiKey(mApiKey);
        req.setModel(Build.MODEL);
        req.setIdentifier(device_id);
        req.setAdid("");
        req.setOsType(OsType.ANDROID);
        req.setOsVersion(String.valueOf(Build.VERSION.RELEASE));

        String deviceId = getDeviceId(req);
        return new GetUserResult(deviceId);
    }

    private String getDeviceId(DeviceData.DeviceRequest req) {
        if ( req.getAdid().equals("") && req.getIdentifier().equals("") ) {
            EncryptionSHA512 encryption_sha = new EncryptionSHA512();
            String sha_id = encryption_sha.encrypt(Build.SERIAL);
            req.setShaId(sha_id);
        }
        if ( SpotService.spotService == null ) {

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
//                e.printStackTrace();
            }
            return null;
        }
        DeviceData.DeviceResponse result = SpotService.spotService.requestRemoteRegistDevice(req);

        if ( result == null )
            return null;

        if ( result.getData() == null )
            return null;

        return result.getData().getDeviceUuid().toString();
    }
}
