package com.zeroweb.auth;


/**
 * Created by USER on 2017-08-31.
 */

public class RealstepAuth {
    private static RealstepAuth instance = null;
    private static final String RSI_TAG = "rsi service : ";

    private RealstepAuth() {}

    public static RealstepAuth getInstance() {
        if ( instance == null )
            instance = new RealstepAuth();
        return instance;
    }

    public RealstepUser getCurrentUser() {
        return new RealstepUser();
    }
}
