package com.zeroweb.rsi.support;

import com.zeroweb.rsi.vo.SpotData;

import java.util.Date;

/**
 * Created by Lee on 2016-11-09.
 */

public interface MoveListener {
    public abstract void onComplete(SpotData result, Date lastCheckInTime, float similarity);
}
