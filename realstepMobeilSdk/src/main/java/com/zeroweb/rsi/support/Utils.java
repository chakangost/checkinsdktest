package com.zeroweb.rsi.support;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.zeroweb.device.wifi.WifiData;
import com.zeroweb.rsi.Setting;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Lee on 2016-11-03.
 */

public class Utils {
    public static void delayTime(long delay) {
        try {
            TimeUnit.MILLISECONDS.sleep(delay);
        } catch(Exception e) {
//            Log.e("CallbackService", e.getLocalizedMessage(), e);
        }
    }

    public static String getVersion(Context context) {
        PackageInfo pi = null;
        String version = "";
        try {
            pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String[] temp = pi.versionName.split("\\.");
            for(int i=0;i<temp.length;i++) {
                version += temp[i];
            }
        } catch (PackageManager.NameNotFoundException e) {
        }

        return version;
    }

    public static String getVersion() {
        String version = Setting.getInstance().getVersion();
//        try {
//            String[] temp = BuildConfig.VERSION_NAME.split("\\.");
//            for(int i=0;i<temp.length;i++) {
//                version += temp[i];
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        return version;
    }

    public static String getVersion(String defaultVersion) {
        if (defaultVersion != null && !"".equals(defaultVersion))
            return defaultVersion;
        else
            return getVersion();
    }

    /**
     * 두 지점간의 거리 계산
     *
     * @param lat1 지점 1 위도
     * @param lon1 지점 1 경도
     * @param lat2 지점 2 위도
     * @param lon2 지점 2 경도
     * @param unit 거리 표출단위
     * @return
     */
    public static double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));

        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;

        if (unit == "kilometer") {
            dist = dist * 1.609344;
        } else if(unit == "meter"){
            dist = dist * 1609.344;
        }

        return (dist);
    }

    // This function converts decimal degrees to radians
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    // This function converts radians to decimal degrees
    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    public static double wifiSimilarity(List<WifiData> oldList, List<WifiData> newList) {
        List<WifiData> src = null;
        List<WifiData> tar = null;

        if (oldList.size() >= newList.size()) {
            src = new ArrayList<>(oldList);
            tar = new ArrayList<>(newList);
        } else {
            src = new ArrayList<>(newList);
            tar = new ArrayList<>(oldList);
        }

        int same = 0;

        for (WifiData srcItem : src) {
            for (WifiData tarItem : tar) {
                if (srcItem.getBssid().equals(tarItem.getBssid())) {
                    same++;
                    break;
                }
            }
        }

        double similar = 0;

        if (same > 0) {
            similar = Math.round(same / (double)src.size() * 100.0) / 100.0;
            return similar;
        } else {
            return 0;
        }
    }
}
