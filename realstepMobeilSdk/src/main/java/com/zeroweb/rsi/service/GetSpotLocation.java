package com.zeroweb.rsi.service;

import android.content.Context;
import android.os.AsyncTask;

import com.zeroweb.rsi.Realstep;
import com.zeroweb.rsi.support.MoveListener;
import com.zeroweb.rsi.support.Utils;
import com.zeroweb.rsi.vo.SpotData;
import com.zeroweb.rsi.vo.VisitData;

import java.io.IOException;

/**
 * Created by Lee on 2016-11-09.
 */

public class GetSpotLocation extends AsyncTask<Void, Void, VisitData> {
    public MoveListener listener = null;
    private static Context mContext;

    public GetSpotLocation(final Context context, MoveListener listener) {
        this.listener = listener;
        this.mContext = context;
    }

    @Override
    protected VisitData doInBackground(Void... params) {
        int count = 0;

        SpotService_.intent(mContext).checkAuth().start();

        while("".equals(SpotService.mLastLocation)) {
            Utils.delayTime(1000);
            count++;

            if (count > 120000)
                return null;
        }
        try {
            return Realstep.getLastVisitData();
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(VisitData result) {
        if ( result == null )
            return;
        listener.onComplete(result.getSpot(), result.getLastCheckInTime(), SpotService.mLastLocationSimilarity);
    }
}