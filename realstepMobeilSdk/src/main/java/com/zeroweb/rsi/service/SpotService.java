package com.zeroweb.rsi.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.zeroweb.device.wifi.Wifi;
import com.zeroweb.device.wifi.WifiData;
import com.zeroweb.device.wifi.WifiListener;
import com.zeroweb.prefs.DevicePrefs;
import com.zeroweb.rsi.Setting;
import com.zeroweb.rsi.base.Prefs_;
import com.zeroweb.rsi.exception.AcceptableException;
import com.zeroweb.rsi.exception.RequiresUpgradeException;
import com.zeroweb.rsi.exception.SystemMaintenanceModeException;
import com.zeroweb.rsi.remote.Remote;
import com.zeroweb.rsi.remote.RemoteIntentService;
import com.zeroweb.rsi.support.Utils;
import com.zeroweb.rsi.util.EncryptionSHA512;
import com.zeroweb.rsi.vo.DeviceData;
import com.zeroweb.rsi.vo.LocateData;
import com.zeroweb.rsi.vo.LocateResultReceiver;
import com.zeroweb.rsi.vo.VisitData;
import com.zeroweb.rsi.vo.VisitDataV2;
import com.zeroweb.rsi.vo.enums.OsType;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.ServiceAction;
import org.androidannotations.annotations.sharedpreferences.Pref;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesWithFallbackProvider;

@EIntentService
public class SpotService extends RemoteIntentService implements LocationListener {
    public static SpotService spotService = null;
    private static String mApiKey = "";
    private static String mServerVersion = "";
    private LocationGooglePlayServicesWithFallbackProvider mLocationProvider;
    private static Context mAppContext;
    private static List<WifiData> mLastWifiScan;

    private static WifiManager mWifiManager;
    private static Boolean mWifiScanningInWait = false;

    public static String mLastLocation = "";
    public static float mLastLocationSimilarity = -1;

    public static boolean isForeground = false;

    public VisitData.VisitResponse result;
    public VisitDataV2.VisitResponse resultV2;

    static int lastTryMin = 60;

    private final static Object transaction = new Object();
    public static boolean isRun = false;

    public static void setIsRun(boolean isRun) {
        synchronized (transaction) {
            SpotService.isRun = isRun;
        }
    }

    public static boolean getIsRun() {
        synchronized (transaction) {
            return SpotService.isRun;
        }
    }

    @Pref
    public Prefs_ prefs;

    private boolean isRegistDeviceId = true;
    private boolean isRegistADID = true;

    public SpotService() {
        super("com.zeroweb.rsi.service.SpotService");
        spotService = this;
    }

    @ServiceAction
    void checkAuth() {
        // 실행 중이면, 이번 실행을 건너 뜀
        if (getIsRun())
            return;

        setIsRun(true);

        mAppContext = getApplicationContext();
        ApplicationInfo ai;
        try {
            ai = mAppContext.getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            if (ai.metaData != null) {
                isRegistADID = ai.metaData.getBoolean("isRegistADID");
                isRegistDeviceId = ai.metaData.getBoolean("isRegistDeviceId");
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Setting.getInstance().setRegistDeviceId(isRegistDeviceId);
        getKeyString();                                                                             // 앱에 등록된 키를 가져온다.\

        if (mApiKey == null || "".equals(mApiKey)) {
            setIsRun(false);
            return;
        }

        serverInit(Utils.getVersion(mServerVersion));
        remote.apiKey = mApiKey;

        if (isForeground || prefs.deviceUuid().get() == null || "".equals(prefs.deviceUuid().get()) || "-1".equals(prefs.deviceUuid().get().toString())) {
            registDevice();
        }
        scanOnce();
    }

    @ServiceAction
    public void scanOnce() {
        try {
            // 현위치에 대해 스캔
            Wifi.getInstance(getApplicationContext(), new WifiListener() {
                @Override
                public void scanComplete(List<WifiData> list) { }

                @Override
                public void scanCompleteAvg(List<WifiData> list) {

                        // 현위치의 이동을 확인
                        if (mLastWifiScan != null) {
                            double similar = Utils.wifiSimilarity(mLastWifiScan, list);

                            if (similar >= 0.8) {
                                Log.d("com.zeroweb.checkin", "NO MOVE PROCESS SKIP (Size " + list.size() + " Similar " + Double.parseDouble(String.format("%.2f", similar)) + ")");
                                SpotService.setIsRun(false);
                                return;
                            }
                        }

                        mLastWifiScan = new ArrayList<WifiData>(list);

                        // 데이터 전송
                        visitRequestV2(mLastWifiScan);

                        SpotService.setIsRun(false);

                }

                @Override
                public void detectAp(WifiData data) { }
            }).startScan();
            // 서버로 위치정보 전송
        } catch (Exception e) {
            // e.printStackTrace();
            Log.d("com.zeroweb.checkin", "Exception 2 : "+e.getClass().getName());
            SpotService.setIsRun(false);
        }
    }

    @Background
    void visitRequest(List<WifiData> wData) {
        try {
            VisitData.VisitRequest req = new VisitData.VisitRequest();
            req.setDeviceUuid(UUID.fromString(prefs.deviceUuid().get()));
            req.setWlanDatas(wData);
            req.setWlanScanType("FIVE_MEDIAN");

            result = getRemote(Utils.getVersion(mServerVersion), mApiKey).visitRequest(req);
            if ( result == null || result.getData() == null)    {
                Log.d("com.zeroweb.checkin", "Failed to get response");
                return;// 인터넷이 안될 경우, 작업을 진행하지 않는다
            }

            String json = new ObjectMapper().writeValueAsString(result);
            prefs.lastResult().put(json);
            mLastLocation = json;
            if (result != null && result.getData().getSimilarity() != null )
                mLastLocationSimilarity = result.getData().getSimilarity();

            // Log.d(RSI_TAG, "VisitRequest : " + reqJson);

        } catch (RequiresUpgradeException upgrade) {
            Log.d("com.zeroweb.checkin", "Exception: RequiresUpgradeException");
        } catch (SystemMaintenanceModeException system) {
            Log.d("com.zeroweb.checkin", "Exception: SystemMaintenanceModeException");
        } catch (AcceptableException system) {
            Log.d("com.zeroweb.checkin", "Exception: AcceptableException");
        } catch (Exception e) {
            // e.printStackTrace();
            Log.d("com.zeroweb.checkin", "Exception 3 : "+e.getClass().getName());
        }
    }

    @Background
    void visitRequestV2(List<WifiData> wData) {
        try {
            VisitDataV2.VisitRequest req = new VisitDataV2.VisitRequest();
            req.setDeviceUuid(UUID.fromString(prefs.deviceUuid().get()));
            req.setWlanData(wData);
            req.setWlanScanType("FIVE_MEDIAN");

            resultV2 = getRemote(Utils.getVersion(mServerVersion), mApiKey).visitRequestV2(req);
            if ( resultV2 == null || resultV2.getData() == null)    {
                Log.d("com.zeroweb.checkin", "Failed to get response");
                return;// 인터넷이 안될 경우, 작업을 진행하지 않는다
            }

            String json = new ObjectMapper().writeValueAsString(resultV2);
            prefs.lastResult().put(json);
            mLastLocation = json;

            //Log.d(RSI_TAG, "VisitRequest : " + reqJson);
            //Log.d("+HGKIM", "VisitResponse : " + json);

        } catch (RequiresUpgradeException upgrade) {
            Log.d("com.zeroweb.checkin", "Exception: RequiresUpgradeException");
        } catch (SystemMaintenanceModeException system) {
            Log.d("com.zeroweb.checkin", "Exception: SystemMaintenanceModeException");
        } catch (AcceptableException system) {
            Log.d("com.zeroweb.checkin", "Exception: AcceptableException");
        } catch (Exception e) {
            // e.printStackTrace();
            Log.d("com.zeroweb.checkin", "Exception 4: "+e.getClass().getName());
        }
    }

    public synchronized static void locateOnce(Context context, final LocateResultReceiver locateResultReceiver) {

        DevicePrefs prefs = DevicePrefs.getInstance(context);

        if (prefs.getDeviceUuid() == null || "".equals(prefs.getDeviceUuid()) || "-1".equals(prefs.getDeviceUuid())) {
            locateResultReceiver.onFailure();
            return;
        }
        final String deviceUuid = prefs.getDeviceUuid();
        IntentFilter filter = new IntentFilter("android.net.wifi.SCAN_RESULTS");
        filter.addAction("android.net.wifi.STATE_CHANGE");
        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                context.unregisterReceiver(this);
                if (!mWifiScanningInWait) {
                    return;
                }
                mWifiScanningInWait = false;
                ArrayList<WifiData> wifiList = new ArrayList<>();
                for (ScanResult result : mWifiManager.getScanResults()) {
                    WifiData wifiItem = new WifiData();
                    wifiItem.setSsid(result.SSID);
                    wifiItem.setBssid(result.BSSID);
                    wifiItem.setCapabilities(result.capabilities);
                    wifiItem.setLevel(result.level);
                    wifiItem.setFrequency(result.frequency);
                    wifiList.add(wifiItem);
                }

                VisitData.VisitRequest req = new VisitData.VisitRequest();
                req.setWlanScanType("ONCE");
                req.setDeviceUuid(UUID.fromString(deviceUuid)); // UUID.fromString(deviceUuid));
                req.setWlanDatas(wifiList);

                LocateRequestTask reqTask = new LocateRequestTask();

                reqTask.setRemote(remote);
                reqTask.setLocateResultReceiver(locateResultReceiver);
                reqTask.execute(req);

            }
        }, filter);

        mWifiScanningInWait = true;
        if (mWifiManager == null) {
            mWifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        }

        mWifiManager.startScan();
    }

    public DeviceData.DeviceResponse requestRemoteRegistDevice(DeviceData.DeviceRequest req) {
        return getRemote(Utils.getVersion(mServerVersion), mApiKey).registDevice(req);
    }

    void requestRegistDevice(DeviceData.DeviceRequest req) {

        if ( req.getAdid().equals("") && req.getIdentifier().equals("") ) {
            EncryptionSHA512 encryption_sha = new EncryptionSHA512();
            String sha_id = encryption_sha.encrypt(Build.SERIAL);
            req.setShaId(sha_id);
        }

        DeviceData.DeviceResponse result = getRemote(Utils.getVersion(mServerVersion), mApiKey).registDevice(req);    // deviceId 를 가져온다.

        if ( result == null )
            return;

        prefs.deviceUuid().put(result.getData().getDeviceUuid().toString());                                   // 세션에 deviceId 를 저장

        SpotService.setIsRun(false);
    }

    @Background
    void registDevice() {

        try {
            String device_id = "";
            if ( this.isRegistDeviceId ) {
                device_id = Build.SERIAL;
            }

            DeviceData.DeviceRequest req = new DeviceData.DeviceRequest();

            if (!prefs.deviceUuid().get().equals("")){
                req.setDeviceUuid(UUID.fromString(prefs.deviceUuid().get()));
            }

            req.setApiKey(mApiKey);
            req.setModel(Build.MODEL);
            req.setIdentifier(device_id);
            req.setAdid("");
            req.setOsType(OsType.ANDROID);
            req.setOsVersion(String.valueOf(Build.VERSION.RELEASE));

            final DeviceData.DeviceRequest final_req = req;

            if(this.isRegistADID && GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(mAppContext) == ConnectionResult.SUCCESS) {
                AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
                    @Override
                    protected String doInBackground(Void... params) {
                        AdvertisingIdClient.Info idInfo = null;
                        try {
                            idInfo = AdvertisingIdClient.getAdvertisingIdInfo(getApplicationContext());
                        } catch (GooglePlayServicesNotAvailableException e) {
                            e.printStackTrace();
                        } catch (GooglePlayServicesRepairableException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        String advertId = null;
                        try{
                            advertId = idInfo.getId();
                        }catch (Exception e){
                        }
                        return advertId;
                    }
                    @Override
                    protected void onPostExecute(String advertId) {
                        if (advertId == null) advertId = "";
                        final_req.setAdid(advertId);

                        new Thread() {
                            public void run() {
                                // 분 단위로 device 등록 시도.
                                long time = System.currentTimeMillis();
                                Date date = new Date(time);
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
                                String simpMin = simpleDateFormat.format(date).split(":")[1];

                                if (Integer.parseInt(simpMin) != lastTryMin){
                                    requestRegistDevice(final_req);
                                    lastTryMin = Integer.parseInt(simpMin);
                                }

                                SpotService.setIsRun(false);
                            }
                        }.start();
                    }
                };
                task.execute();
            } else {
                requestRegistDevice(req);
                SpotService.setIsRun(false);
            }
        } catch (RequiresUpgradeException upgrade) {
            SpotService.setIsRun(false);
        } catch (SystemMaintenanceModeException system) {
            SpotService.setIsRun(false);
        } catch (AcceptableException system) {
            SpotService.setIsRun(false);
        } catch (Exception e) {
            SpotService.setIsRun(false);
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {}

    void getKeyString() {
        // 앱에 등록된 키를 가져온다.
        try {
            ApplicationInfo ai = getPackageManager().getApplicationInfo(this.getPackageName(), PackageManager.GET_META_DATA);
            if ( ai == null )
                return;
            Bundle bundle = ai.metaData;
            if ( bundle == null )
                return;
            mApiKey = bundle.getString("com.zeroweb.rsi.key");
            mServerVersion = Setting.getInstance().getVersion();
        } catch (PackageManager.NameNotFoundException e) {
//            Log.e(RSI_TAG, "Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (NullPointerException e) {
//            Log.e(RSI_TAG, "Failed to load meta-data, NullPointer: " + e.getMessage());
        }
    }

    @Override
    public void onLocationChanged(Location location) { }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) { }

    @Override
    public void onProviderEnabled(String s) { }

    @Override
    public void onProviderDisabled(String s) { }

    private static class LocateRequestTask extends AsyncTask<VisitData.VisitRequest, Void, LocateData> {

        private Exception exception;
        private Remote remote;
        private LocateResultReceiver locateResultReceiver;

        public void setRemote(Remote remote) {
            this.remote = remote;
        }

        public void setLocateResultReceiver(LocateResultReceiver locateResultReceiver) {
            this.locateResultReceiver = locateResultReceiver;
        }

        @Override
        protected LocateData doInBackground(VisitData.VisitRequest... visitRequests) {
            try {
                return this.remote.locateRequest(visitRequests[0]);
            } catch (Exception e) {
                this.exception = e;
                return null;
            }
        }

        protected void onPostExecute(LocateData response) {
            if (response == null) {
                locateResultReceiver.onFailure();
                return;
            }
            Location location = new Location("Realstep");
            location.setLatitude(response.getData().getLatitude());
            location.setLongitude(response.getData().getLongitude());
            locateResultReceiver.onSuccess(location);
        }
    }
//    //retrofit
//    class BaseResponse {
//        @SerializedName("state")
//        @Expose
//        String state;
//    }
//
//    interface CheckinTestService {
//        @GET("/checkin/background/main")
//        Call<BaseResponse> main();
//    }
}
