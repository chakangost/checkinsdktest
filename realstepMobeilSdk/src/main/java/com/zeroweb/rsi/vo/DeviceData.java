package com.zeroweb.rsi.vo;

import com.zeroweb.rsi.remote.Response;
import com.zeroweb.rsi.vo.enums.GenderType;
import com.zeroweb.rsi.vo.enums.OsType;

import java.util.Date;
import java.util.UUID;

import lombok.Data;

@Data
public class DeviceData {
    UUID deviceUuid;
    String model;			// 모델명 또는 명칭
    String identifier;		// 기기 식별자
    String adid;            // 기기 식별자 (ADID)
    String shaId;          // 기기 식별자 (sha)
    String osType;			// 기기 운영체계 유형
    String osVersion;		// 운영체계 버전
    GenderType gender;		// 성별
    int age;			    // 나이
    boolean disabled;       // 사용중지(기기 등록 해제시 true)
    Date registeredDate;	// 등록 일시
    Date updatedDate;		// 갱신 일시

    Boolean enableWifi;

    @Data
    public static class DeviceRequest {
        UUID deviceUuid;

        String apiKey;			// api 키

        String model;			// 모델명 또는 명칭
        String identifier;		// 기기 식별자
        String adid;            // 기기 식별자 (ADID)
        String shaId;          // 기기 식별자 (sha)

        OsType osType;			// 기기 운영체계 유형
        String osVersion;		// 운영체계 버전

        public void display() {
//            System.out.println("+swpark");
//            System.out.println("====================================");
//            System.out.println("deviceUuid : " + this.deviceUuid);
//            System.out.println("apiKey : " + this.apiKey);
//            System.out.println("model : " + this.model);
//            System.out.println("identifier : " + this.identifier);
//            System.out.println("ADID : " + this.adid);
//            System.out.println("shaId : " + this.shaId);
//            System.out.println("osType : " + this.osType);
//            System.out.println("osVersion : " + this.osVersion);
//            System.out.println("====================================");
        }
    }

    public static class DeviceResponse extends Response<DeviceData> {
    }

}