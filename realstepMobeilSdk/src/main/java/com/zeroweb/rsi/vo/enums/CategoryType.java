package com.zeroweb.rsi.vo.enums;

import com.zeroweb.rsi.base.BaseEnum;

public enum CategoryType implements BaseEnum {
	// 카테고리 유형
	MAIN("대분류")			// 대분류
	,SUB("상세분류")		// 상세분류
	;

	String descr;
	
	CategoryType(String descr) {
		this.descr = descr;
	}

	@Override
	public String getDescr() {
		return this.descr;
	}

	@Override
	public String getValue() {
		return this.name();
	}

}
