package com.zeroweb.rsi.vo;

import com.zeroweb.rsi.remote.Response;

import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * Created by Lee on 2016-09-22.
 */

@Data
public class CategoryData {
    Long categoryId;
    String name;					// 카테고리 명
    String parentName;              // 부모 카테고리
//    String parentCategoryName;		// 상위 카테고리 명
    String classification;	// 카테고리 구분
    boolean disabled;				// 사용중지(기기 등록 해제시 true)
    boolean leaf;					// 단말노드(leaf) 여부

    Date createdDate;				// 등록 일시
    Date updatedDate;				// 갱신 일시

    @Data
    public static class CategoryRequest {
        Long categoryId;
        String keyword;				// 검색 키워드
    }

    public static class CategoryResponse extends Response<CategoryData> {
    }

    public static class CategoryListResponse extends Response<List<CategoryData>> {
    }
}
