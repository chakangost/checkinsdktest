package com.zeroweb.rsi.vo;

import android.location.Location;
import android.util.Log;

public class LocateResultReceiver {
    public void onSuccess(Location location) {
        Log.d("khj", "Receiver : " + location.getLatitude()+","+location.getLongitude());
    }

    public void onFailure() {
        Log.d("khj", "Unexpected failure during finding lcoation.");
    }
}
