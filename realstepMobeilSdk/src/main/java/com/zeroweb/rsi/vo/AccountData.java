package com.zeroweb.rsi.vo;

import com.zeroweb.rsi.remote.Response;

import java.util.Date;
import java.util.UUID;

import lombok.Data;

@Data
public class AccountData {

    Long userId;
    String authKey;					// 인증 키

    String username;				// 사용자 계정(로그인 ID)
    String fullname;				// 사용자 이름
    String tempPasswd;				// 임시비밀번호

    String companyName;				// 소속
    String email;					// 이메일
    String phone;					// 전화번호

    String status;				    // 계정상태

    Date createdDate;				// 등록 일시
    Date updatedDate;				// 갱신 일시
    Date lastLoggedInDate;			// 최근 로그인 일시

    @Data
    public static class AccountRequest {

        String apiKey;

        UUID deviceUuid;
        Long userId;
        String username;			// 사용자 계정(로그인 ID)
        String password;			// 비밀번호

        String newPasswd;			// 새 비밀번호
    }

    public static class AccountResponse extends Response<AccountData> {
    }
}