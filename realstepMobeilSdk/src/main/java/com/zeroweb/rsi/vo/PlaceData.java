package com.zeroweb.rsi.vo;

import com.zeroweb.rsi.remote.Response;

import java.util.List;
import java.util.UUID;

import lombok.Data;

@Data
public class PlaceData {
    UUID placeId;
    private String name;								// 스팟 명
    private Double lat;									// 위도
    private Double lng;									// 경도
    private String floor;								// 층
    private String inout;
    private String userId;

    String distAddr;									// 주소
    String roadAddr;									// 도로명 주소

    List<TagData> tags;

    @Data
    public static class TagData {
        String id;
        String name;
        String type;
    }

    public static class SpotResponse extends Response<PlaceData> {

    }

}
