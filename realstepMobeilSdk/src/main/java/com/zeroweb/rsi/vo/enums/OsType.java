package com.zeroweb.rsi.vo.enums;

import com.zeroweb.rsi.base.BaseEnum;

public enum OsType implements BaseEnum {
	ANDROID("Android"), IOS("iOS"), WINDOWS("Windows"), BB("BlackBerry");

	String descr;
	
	OsType(String descr) {
		this.descr = descr;
	}

	@Override
	public String getDescr() {
		return this.descr;
	}

	@Override
	public String getValue() {
		return this.name();
	}

}
