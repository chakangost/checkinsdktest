package com.zeroweb.rsi.vo;

import android.util.Log;

import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by park on 2018-04-23.
 */
@Data
@NoArgsConstructor
public class LocateData {

    LocateResult data;

    @Data
    public static class LocateResult {
        double latitude;
        double longitude;
    }
}

