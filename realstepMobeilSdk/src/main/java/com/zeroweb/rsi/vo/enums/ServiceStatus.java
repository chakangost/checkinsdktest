package com.zeroweb.rsi.vo.enums;

import com.zeroweb.rsi.base.BaseEnum;

public enum ServiceStatus implements BaseEnum {
	// 서비스(Service) 상태
	PENDING("승인대기중"),
	ACTIVE("승인완료"),
	INACTIVE("사용중지"),
	REJECT("승인거부")		
	;

	String descr;
	
	ServiceStatus(String descr) {
		this.descr = descr;
	}

	@Override
	public String getDescr() {
		return this.descr;
	}

	@Override
	public String getValue() {
		return this.name();
	}

}
