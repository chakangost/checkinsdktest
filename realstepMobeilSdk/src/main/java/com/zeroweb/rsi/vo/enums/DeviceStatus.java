package com.zeroweb.rsi.vo.enums;

import com.zeroweb.rsi.base.BaseEnum;

public enum DeviceStatus implements BaseEnum {
	// 장치(Device) 상태
		ACTIVE("활성")
		, INACTIVE("비활성")
		, STOLEN("도난")		
		;

	String descr;
	
	DeviceStatus(String descr) {
		this.descr = descr;
	}

	@Override
	public String getDescr() {
		return this.descr;
	}

	@Override
	public String getValue() {
		return this.name();
	}

}
