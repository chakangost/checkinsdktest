package com.zeroweb.rsi.vo;

import com.zeroweb.device.wifi.WifiData;
import com.zeroweb.rsi.remote.Response;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
public class VisitDataV2 {
	
	PlaceData place;
	Date lastCheckInTime;
	Boolean enableWifi;
	
	@Data 
	public static class WlanData {
		
		String ssid;							// ssid
		
		String bssid;							// Basic service set identification(ap mac addr.)
		
		String capabilities;					// 인증방식(?)

		Integer frequency;						// 주파수

		Integer level;							// 수신레벨(?)
	}
	
	@Data 
	public static class BleData {
		
		String name;							// 기기명
		
		String bdAddr;							// bt mac address
		
		String uuid;							// beacon uuid
		
		Integer major;							// beacon major
		
		Integer minor;							// beacon minor

		Integer rssi;							// 수신전개강도
		
		Integer txPower;						// 신호세기
	}
	
	@Data 
	public static class VisitRequest {
		
		Long spotId;					// 지점 식별자
		
		UUID deviceUuid;					// 기기 식별자
		String adId;
		Double lat;						// 위도
		Double lng;						// 경도
		
	    List<WifiData> wlanData;		// 무선랜 정보
	    
		Date now;						// 현재 일시

		String wlanScanType;

	}
	
	public static class VisitResponse extends Response<VisitDataV2> {

	}
	
	public static class VisitListResponse extends Response<List<VisitDataV2>> {

	}
	
}
