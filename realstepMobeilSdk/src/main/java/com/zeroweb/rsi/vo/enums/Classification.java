package com.zeroweb.rsi.vo.enums;

import com.zeroweb.rsi.base.BaseEnum;

public enum Classification implements BaseEnum {
	DEPTH_1("대분류"), 
	DEPTH_2("중분류"), 
	DEPTH_3("소분류"), 
	DEPTH_4("상세분류");

	String descr;
	
	Classification(String descr) {
		this.descr = descr;
	}

	@Override
	public String getDescr() {
		return this.descr;
	}

	@Override
	public String getValue() {
		return this.name();
	}

}
