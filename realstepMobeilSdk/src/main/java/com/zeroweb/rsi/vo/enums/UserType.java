package com.zeroweb.rsi.vo.enums;

import com.zeroweb.rsi.base.BaseEnum;

public enum UserType implements BaseEnum {
	ADMIN("시스템 관리자"), MANAGER("관리자"), USER("사용자"), WORKER("작업자"), DEVELOPER("개발자");

	String descr;
	
	UserType(String descr) {
		this.descr = descr;
	}

	@Override
	public String getDescr() {
		return this.descr;
	}

	@Override
	public String getValue() {
		return this.name();
	}

}
