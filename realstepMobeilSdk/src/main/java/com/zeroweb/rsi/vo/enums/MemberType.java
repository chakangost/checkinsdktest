package com.zeroweb.rsi.vo.enums;

import com.zeroweb.rsi.base.BaseEnum;

public enum MemberType implements BaseEnum {
	SILVER("실버"), GOLD("골드"), DIAMOND("다이아몬드");

	String descr;
	
	MemberType(String descr) {
		this.descr = descr;
	}

	@Override
	public String getDescr() {
		return this.descr;
	}

	@Override
	public String getValue() {
		return this.name();
	}

}
