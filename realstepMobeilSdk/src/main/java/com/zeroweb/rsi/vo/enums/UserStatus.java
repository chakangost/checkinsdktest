package com.zeroweb.rsi.vo.enums;

import com.zeroweb.rsi.base.BaseEnum;

public enum UserStatus implements BaseEnum {
	// 계정(account) 상태
		ACTIVE("사용")
		,INACTIVE("사용중지")

		,PENDING("승인대기")
		,LEAVE("퇴사/탈퇴")
		;

	String descr;
	
	UserStatus(String descr) {
		this.descr = descr;
	}

	@Override
	public String getDescr() {
		return this.descr;
	}

	@Override
	public String getValue() {
		return this.name();
	}

}
