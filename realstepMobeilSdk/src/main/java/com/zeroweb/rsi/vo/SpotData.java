package com.zeroweb.rsi.vo;

import com.zeroweb.rsi.remote.Response;
import com.zeroweb.device.wifi.WifiData;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import lombok.Data;

@Data
public class SpotData {
	private String userFullname;
	private Long spotId;								// Spot Unique Id
	private Long userId;								// 사용자 식별자
	private Long categoryId;							// 카테고리 식별자

	private String categoryName;

	UUID deviceUuid;
	private String name;								// 스팟 명
	String distAddr;									// 주소
	String roadAddr;									// 도로명 주소
	private String floor;								// 층
	private String tag;	 								// 태그(콤마로 구분)

	private Double lat;									// 위도
	private Double lng;									// 경도

    private List<WifiData> wlanDatas;					// 무선랜 정보

	private boolean disabled;							// 사용중지(기기 등록 해제시 true)

	private Date registeredDate;						// 등록 일시
	private Date updatedDate;							// 갱신 일시

	String partnerId;									// 가맹점ID (제로웹)
	String partnerName;									// 가맹점이름 (제로웹)

	/*@Data
	public static class WlanData {

		private String ssid;							// ssid

		private String bssid;							// Basic service set identification(ap mac addr.)

		private String capabilities;					// 인증방식(?)

		private int frequency;							// 주파수

		private int level;								// 수신레벨(?)
	}

	@Data
	public static class BleData {

		private String name;							// 기기명

		private String bdAddr;							// bt mac address

		private String uuid;							// beacon uuid

		private int major;								// beacon major

		private int minor;								// beacon minor

		private int rssi;								// 수신전개강도

		private int txPower;							// 신호세기
	}*/

	@Data
	public static class CellularData {

		private int cellId;								// Cell ID

		private int lac;								// LAC(Location Area Code)

		private int psc;								// PSC(Primary Scrambling Code)

	}

	@Data
	public static class Bounds {

		private Double ba;
		private Double ha;
		private Double fa;
		private Double ga;

	}

	@Data
	public static class SpotRequest {

		Long spotId;
		UUID deviceUuid;
		Long userId;			// 사용자 식별자
		Long categoryId;		// 카테고리 식별자

		String name;			// 스팟 명
		String distAddr;		// 주소
		String roadAddr;		// 도로명 주소
		String floor;			// 층
		String tag;	 			// 태그(콤마로 구분)

		Double lat;				// 위도
		Double lng;				// 경도

	    List<WifiData> wlanDatas;			// 무선랜 정보
//	    List<CellularData> cellularDatas;	// 이동통신 기지국 정보

	    boolean disabled;		// 사용중지(기기 등록 해제시 true)

		String partnerId;		// 가맹점ID (제로웹)
		String partnerName;		// 가맹점이름 (제로웹)

		Date registeredDate;	// 등록 일시
		Date updatedDate;		// 갱신 일시

		// 검색용
		String keyword;			// 키워드
//		String neLatlng;		// 북동쪽 끝 좌표
//		String swLatlng;		// 남서쪽 끝 좌표
		Bounds bounds;			// 지도 영역

	}

	public static class SpotResponse extends Response<SpotData> {
	}

	public static class SpotListResponse extends Response<List<SpotData>> {
	}

}
