package com.zeroweb.rsi;

import lombok.Data;

/**
 * Created by USER on 2017-08-25.
 */

@Data
public class Setting {
    private static Setting instance = null;
    private boolean enableWifi = false;
    private boolean isRegistDeviceId = false;
    private boolean isRegistADID = false;
    private String host = "https://realstep-mobile.zeroweb.kr/app"; // 제로웹 실서버
//    private String host = "http://54.180.100.209/app"; // 제로웹 스테이징
    //private String host = "http://192.168.0.5:8001/app";
    private String version = "109";

    private Setting() {}

    public static Setting getInstance() {
        if ( instance == null )
            instance = new Setting();
        return instance;
    }
}
