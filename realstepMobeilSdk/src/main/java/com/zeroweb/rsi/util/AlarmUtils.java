package com.zeroweb.rsi.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.zeroweb.rsi.receiver.AlarmOneMinuteBroadcaseReceiver;

/**
 * Created by USER on 2017-10-17.
 */

public class AlarmUtils {
    private int loopTime = 60 * 1000;

    private static AlarmUtils instance = null;

    public static AlarmUtils getInstance() {
        if ( instance == null )
            instance = new AlarmUtils();
        return instance;
    }

    private void startAlarm(Context context, PendingIntent pendingIntent, int delay) {
        AlarmManager manager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + delay, pendingIntent);
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            manager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + delay, pendingIntent);
        else
            manager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + delay, pendingIntent);
    }

    public void startOneMinuteAlarm(Context context) {
        Intent alarmIntent = new Intent(context, AlarmOneMinuteBroadcaseReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
        startAlarm(context, pendingIntent, loopTime);
    }

    public int getLoopTime() {
        return this.loopTime;
    }

    public void setLoopTime(int loopTime) {
        this.loopTime = loopTime;
    }
}
