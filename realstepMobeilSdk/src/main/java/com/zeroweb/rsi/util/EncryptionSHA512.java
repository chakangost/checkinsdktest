package com.zeroweb.rsi.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by USER on 2017-05-11.
 */

public class EncryptionSHA512 {
    public String encrypt(String data) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-512");
            digest.reset();
            digest.update(("dkanrjskvotmdnjemfksek#$%!625rkskekfk"+data).getBytes());
            StringBuffer sb = new StringBuffer();
            for ( byte b : digest.digest() )
                sb.append(Integer.toHexString(0xff & b));
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
}
