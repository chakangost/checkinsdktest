package com.zeroweb.rsi;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import com.facebook.stetho.Stetho;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zeroweb.auth.GetUserResult;
import com.zeroweb.auth.RealstepAuth;
import com.zeroweb.auth.RealstepUser;
import com.zeroweb.prefs.DevicePrefs;
import com.zeroweb.rsi.receiver.AlarmOneMinuteBroadcaseReceiver;
import com.zeroweb.rsi.service.SpotService;
import com.zeroweb.rsi.service.SpotService_;
import com.zeroweb.rsi.util.AlarmUtils;
import com.zeroweb.rsi.vo.LocateResultReceiver;
import com.zeroweb.rsi.vo.SpotData;
import com.zeroweb.rsi.vo.VisitData;
import com.zeroweb.spot.RealstepSpot;
import com.zeroweb.util.Utility;
import java.io.IOException;

public class Realstep {

    private static CheckInListener checkInListener = null;
    public static String apiKey = "";
    public static Context initContext = null;

    private static void  init(final Context context, CheckInListener _checkInListener) {
        Log.d("com.zeroweb.checkin", "eunho Realstep init");
        initContext = context;

        if (DevicePrefs.getInstance(context).getApiKey().equals("")) {
            apiKey = getApiKeyFromManifest(context);
            DevicePrefs.getInstance(context).setApiKey(apiKey);
            Log.d("com.zeroweb.checkin", "apiKey set!!!!!!!! : " + apiKey);
        }

        // 마시멜로우 이후버전에 대해서 새로운 분기
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!AlarmOneMinuteBroadcaseReceiver.isLaunched)
                Log.d("com.zeroweb.checkin", "eunho Realstep init");
                Utility.getInstance().scheduleJob(context);
            return;
        }

        // 롤리팝 이전에 대해서는 기존 버전으로 구동한다
        SpotService.isForeground = true;
        try {
            SpotService_.intent(context.getApplicationContext()).checkAuth().start();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        if (!AlarmOneMinuteBroadcaseReceiver.isLaunched)
            AlarmUtils.getInstance().startOneMinuteAlarm(context);
        checkInListener = _checkInListener;
    }

    public static void init(final Context context) {
        init(context, null);
    }

    public static void setLoopTime(int loopTime) {
        if ( loopTime <= 0 )
            return;
        AlarmUtils.getInstance().setLoopTime(loopTime);
    }

    public static void configure() {
        RealstepUser realstepUser = RealstepAuth.getInstance().getCurrentUser();
        GetUserResult getUserResult = realstepUser.getUser().getResult();
    }

    public static void requestLocate(final Context context, final LocateResultReceiver locateResultReceiver) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!AlarmOneMinuteBroadcaseReceiver.isLaunched) {
                RealstepSpot.getInstance(context).locateOnce(context, locateResultReceiver);
                return;
            }
        }
        SpotService_.locateOnce(context, locateResultReceiver);
    }

    public static SpotData getLastLocation(Context context) throws IOException {
        if (SpotService.mLastLocation.equals(""))
            return null;

        VisitData.VisitResponse result = new ObjectMapper().readValue(SpotService.mLastLocation, VisitData.VisitResponse.class);
        if ( result == null || result.getData() == null )
            return null;

        return result.getData().getSpot();
    }

    public static VisitData getLastVisitData() throws IOException {
        if ("".equals(SpotService.mLastLocation))
            return null;

        VisitData.VisitResponse result = new ObjectMapper().readValue(SpotService.mLastLocation, VisitData.VisitResponse.class);
        if ( result == null )
            return null;

        return result.getData();
    }

    private static String getApiKeyFromManifest(Context context) {
        String apiKey = null;

        try {
            String e = context.getPackageName();
            ApplicationInfo ai = context.getPackageManager().getApplicationInfo(e, PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            if(bundle != null) {
                apiKey = bundle.getString("com.zeroweb.rsi.key");
            }
        } catch (Exception var6) {
            Log.d("com.zeroweb.checkin", "Caught non-fatal exception while retrieving apiKey: " + var6);
        }
        return apiKey;
    }
}