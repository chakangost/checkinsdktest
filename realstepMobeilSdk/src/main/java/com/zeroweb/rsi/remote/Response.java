package com.zeroweb.rsi.remote;
import lombok.Data;

/**
 * 실행 결과를 포함하는 응답.
 * @author T
 *
 * @param <T>
 */
@Data
public class Response<T> {
    public static final int OK = 0;
    public static final int UNKNOWN = 999;

	T data;
	int status;
	String error;
}


