package com.zeroweb.rsi.remote;

import android.app.IntentService;
import android.content.Intent;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EIntentService;

/**
 * Created by Lee on 2016-11-07.
 */
@EIntentService
public abstract class RemoteIntentService extends IntentService {
    @Bean
    public static Remote remote;

    private static String mVersion;

    public RemoteIntentService(String name) {
        super(name);
    }

    public static void serverInit(String version) {
        if(!"".equals(version) && version != null) {
            mVersion = version;
            remote.init("https://realstep-mobile.zeroweb.kr/app", Integer.parseInt(version));       // 제로웹 실서버
//            remote.init("https://54.180.100.209/app", Integer.parseInt(version));       // 제로웹 Staging
//            remote.init("https://realstep-checkin-dev.zeroweb.cloud/app", Integer.parseInt(version));       // 제로웹 개발서버
//            remote.init("http://192.168.80.219:2000/app", Integer.parseInt(version));       // Carl local 서버
        }
    }

    public Remote getRemote() {
        return remote;
    }

    public static Remote getRemote(String version, String apiKey) {

        if(remote == null) {
            serverInit(version);
            remote.apiKey = apiKey;
        }

        return remote;
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }
}
