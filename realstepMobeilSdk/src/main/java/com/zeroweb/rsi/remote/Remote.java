package com.zeroweb.rsi.remote;

import android.util.Log;
import com.zeroweb.rsi.Setting;
import com.zeroweb.rsi.api.Api;
import com.zeroweb.rsi.exception.AcceptableException;
import com.zeroweb.rsi.exception.RequiresUpgradeException;
import com.zeroweb.rsi.exception.SystemMaintenanceModeException;
import com.zeroweb.rsi.vo.DeviceData;
import com.zeroweb.rsi.vo.LocateData;
import com.zeroweb.rsi.vo.VisitData;
import com.zeroweb.rsi.vo.VisitDataV2;
import org.androidannotations.annotations.EBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import java.io.EOFException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.UnknownHostException;

/**
 * Created by Lee on 2016-09-22.
 */

@EBean(scope = EBean.Scope.Singleton)
public class Remote {
    private static final String TAG = "sdk.Remote";

    private static final int MAX_RETRY = 3;

    public String authKey = "";

    public String apiKey = "";

    public String server = "";

    private int appVersion;

    private RestTemplate rest;

    public Remote() {
    }

    interface TryOnce<T> {
        T tryOnce();
    }

    public <T> T retry(TryOnce<T> r) {
        int retry = MAX_RETRY;
        while (retry > 0) {
            try {
                return r.tryOnce();
            } catch (RuntimeException e) {
                if (e instanceof HttpClientErrorException) {
                    HttpClientErrorException he = (HttpClientErrorException) e;
                    switch (he.getStatusCode()) {
                        case NOT_ACCEPTABLE:
                            throw new AcceptableException();
                        case UPGRADE_REQUIRED:
                            throw new RequiresUpgradeException();
                        case FORBIDDEN:
                            throw new SystemMaintenanceModeException();
                        default:
                            throw e;
                    }
                } else if (e.getCause() != null && e.getCause() instanceof UnknownHostException) {
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e1) {
                    }
                    retry--;
                    if (retry <= 0) throw e;
                } else if (e.getCause() != null && e.getCause() instanceof EOFException) {
                    retry--;
                    if (retry <= 0) throw e;
                } else {
                    throw e;
                }
            }
        }
        throw new IllegalStateException();
    }

    public void init(String server, int appVersion) {
        if (rest != null) return;

        this.server = server;
        this.appVersion = appVersion;

        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory() {
            @Override
            protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
                super.prepareConnection(connection, httpMethod);
                connection.setRequestProperty("AuthKey", Remote.this.authKey);
                connection.setRequestProperty("ApiKey", Remote.this.apiKey);
                connection.setRequestProperty("AppVersion", String.valueOf(Remote.this.appVersion));
            }
        };
        factory.setReadTimeout(10 * 1000);
        factory.setConnectTimeout(3 * 1000);    // 커넥션 타임아웃 3초
        this.rest = new RestTemplate(factory);

        this.rest.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        this.rest.getMessageConverters().add(new FormHttpMessageConverter());
    }

    /**********************************************************/
    /** Services				   		 			 		 **/
    /**********************************************************/

    @Api
    public DeviceData.DeviceResponse registDevice(final DeviceData.DeviceRequest req) {
        try {
            return retry(new TryOnce<DeviceData.DeviceResponse>() {
                public DeviceData.DeviceResponse tryOnce() {
                    req.display();
                    DeviceData.DeviceResponse data = rest.postForObject(URI.create(Remote.this.server + "/api/device/register"), req, DeviceData.DeviceResponse.class);
                    if ( data.getData() != null && data.getData().getEnableWifi() != null ) {
                        Setting.getInstance().setEnableWifi(data.getData().getEnableWifi());
                    }
                    Log.d("com.zeroweb.checkin", "Success 6 : " + data.toString());
                    return data;
                }
            });
        } catch (Exception e) {
//            Log.e("DEVICE_REGIST", e.toString());
            return null;
        }
    }

    @Api
    public VisitData.VisitResponse visitRequest(final VisitData.VisitRequest req) {
        try {
            return retry(new TryOnce<VisitData.VisitResponse>() {
                public VisitData.VisitResponse tryOnce(){
                    VisitData.VisitResponse data = rest.postForObject(URI.create(Remote.this.server + "/api/visit/checkin"), req, VisitData.VisitResponse.class);
                    if(data.getData()!=null&&data.getData().getEnableWifi() !=null) {
                        Setting.getInstance().setEnableWifi(data.getData().getEnableWifi());
                    }
                    Log.d("com.zeroweb.checkin", "Visit Success : " + data.toString());
                    return data;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Api
    public VisitDataV2.VisitResponse visitRequestV2(final VisitDataV2.VisitRequest req) {
        try {
            return retry(new TryOnce<VisitDataV2.VisitResponse>() {
                public VisitDataV2.VisitResponse tryOnce(){
                    VisitDataV2.VisitResponse data = rest.postForObject(URI.create(Remote.this.server + "/api/v2/visit/checkin"), req, VisitDataV2.VisitResponse.class);
                    if(data.getData()!=null&&data.getData().getEnableWifi() !=null) {
                        Setting.getInstance().setEnableWifi(data.getData().getEnableWifi());
                    }
                    Log.d("com.zeroweb.checkin", "Visit2 Success : " + data.toString());
                    return data;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Api
    public LocateData locateRequest(final VisitData.VisitRequest req) {
        try {
            return retry(new TryOnce<LocateData>() {
                public LocateData tryOnce(){
                    HttpHeaders requestHeaders = new HttpHeaders();
                    requestHeaders.setContentType(new MediaType("application","json"));
                    HttpEntity<VisitData.VisitRequest> reqEntity = new HttpEntity<VisitData.VisitRequest>(req, requestHeaders);
                    return rest.postForObject(URI.create(Remote.this.server + "/api/visit/locate"), req, LocateData.class);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}