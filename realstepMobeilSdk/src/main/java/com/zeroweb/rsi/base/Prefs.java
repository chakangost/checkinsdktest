package com.zeroweb.rsi.base;

import com.zeroweb.rsi.vo.VisitData;

import org.androidannotations.annotations.sharedpreferences.DefaultBoolean;
import org.androidannotations.annotations.sharedpreferences.DefaultLong;
import org.androidannotations.annotations.sharedpreferences.DefaultRes;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by Lee on 2016-11-07.
 */

@SharedPref(value=SharedPref.Scope.UNIQUE)
public interface Prefs {
    @DefaultString("")
    String deviceUuid();

    @DefaultString("")
    String apiKey();

    @DefaultBoolean(true)
    boolean firstScan();

    @DefaultString("")
    String lastResult();
}
