package com.zeroweb.rsi.base;

public interface BaseEnum {
    String getDescr();
    String getValue();
}