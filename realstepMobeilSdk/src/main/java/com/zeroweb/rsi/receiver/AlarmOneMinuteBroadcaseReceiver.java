package com.zeroweb.rsi.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.zeroweb.rsi.service.SpotService_;
import com.zeroweb.rsi.util.AlarmUtils;
import com.zeroweb.util.Utility;


public class AlarmOneMinuteBroadcaseReceiver extends BroadcastReceiver {
    public static boolean isLaunched = false;
    private Utility utility = Utility.getInstance();

    @Override
    public void onReceive(Context context, Intent intent) {
        isLaunched = true;

        // 롤리팝 이상 버전에서의 처리
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.d("com.zeroweb.checkin", "AlarmOneMinuteBroadcaseReceiver");
            utility.scheduleJob(context);
            return;
        }

        // 롤리팝 이전 버전에서의 처리
        AlarmUtils.getInstance().startOneMinuteAlarm(context);
        try {
            SpotService_.intent(context.getApplicationContext()).checkAuth().start();
        }
        catch(IllegalStateException e) { // Android Oreo 이상에서 날 수 있는 Exception 처리
            Log.d("com.zeroweb.checkin", "Failed to start background service");
        }
    }
}

