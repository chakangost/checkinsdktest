package com.zeroweb.rsi.exception;

public class RequiresUpgradeException extends RuntimeException {
    private static final long serialVersionUID = -8009175426637686977L;
}