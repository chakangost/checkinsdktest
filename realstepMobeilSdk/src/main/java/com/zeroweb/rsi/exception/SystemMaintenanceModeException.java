package com.zeroweb.rsi.exception;

public class SystemMaintenanceModeException extends RuntimeException {
    private static final long serialVersionUID = -4027769520513090156L;
}