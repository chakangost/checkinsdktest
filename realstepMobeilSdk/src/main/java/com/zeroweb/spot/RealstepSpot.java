package com.zeroweb.spot;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.zeroweb.device.wifi.Wifi;
import com.zeroweb.device.wifi.WifiData;
import com.zeroweb.device.wifi.WifiListener;
import com.zeroweb.prefs.DevicePrefs;
import com.zeroweb.rsi.Setting;
import com.zeroweb.rsi.api.Api;
import com.zeroweb.rsi.exception.AcceptableException;
import com.zeroweb.rsi.exception.RequiresUpgradeException;
import com.zeroweb.rsi.exception.SystemMaintenanceModeException;
import com.zeroweb.rsi.service.SpotService;
import com.zeroweb.rsi.support.Utils;
import com.zeroweb.rsi.util.EncryptionSHA512;
import com.zeroweb.rsi.vo.DeviceData;
import com.zeroweb.rsi.vo.LocateData;
import com.zeroweb.rsi.vo.LocateResultReceiver;
import com.zeroweb.rsi.vo.VisitData;
import com.zeroweb.rsi.vo.VisitDataV2;
import com.zeroweb.rsi.vo.enums.OsType;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import java.io.EOFException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class RealstepSpot {
    private static RealstepSpot instance = null;
    private Setting setting = Setting.getInstance();
    private Context appContext;
    private DevicePrefs devicePrefs;
    private RestTemplate rest;
    private String apiKey = "";
    private String serverVersion = "";
    private boolean isRegistADID = true;
    private boolean isRegistDeviceId = true;
    private static List<WifiData> lastWifiSignal = null;
    private static WifiManager mWifiManager;
    private static Boolean mWifiScanningInWait = false;
    private String TAG = "com.zeroweb.checkin";

    private RealstepSpot(Context context) {
        ApplicationInfo ai = null;
        Bundle bundle = null;

        this.appContext = context;

        try {
            ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            if (ai == null)
                return;
            bundle = ai.metaData;
            if (bundle == null)
                return;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        devicePrefs = DevicePrefs.getInstance(context);
        apiKey = devicePrefs.getApiKey();
        Log.d(TAG, "API Key is : " + apiKey);
        serverVersion = Setting.getInstance().getVersion();

        new InitRestTemplateAsyncTask().execute();

        isRegistADID = ai.metaData.getBoolean("isRegistADID");
        isRegistDeviceId = ai.metaData.getBoolean("isRegistDeviceId");
        Setting.getInstance().setRegistDeviceId(isRegistDeviceId);
        Setting.getInstance().setRegistADID(isRegistADID);

        Log.d(TAG, "RealstepSpot Last");
    }

    public static RealstepSpot getInstance(Context context) {
        if (instance == null)
            instance = new RealstepSpot(context);
        return instance;
    }

    private void initRestTemplate() {
        if (this.rest != null)
            return;

        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory() {
            @Override
            protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
                super.prepareConnection(connection, httpMethod);
                connection.setRequestProperty("ApiKey", apiKey);
                connection.setRequestProperty("AppVersion", setting.getVersion());
            }
        };
        Log.d(TAG, "initRestTemplate : 11");
        factory.setReadTimeout(10 * 1000);
        factory.setConnectTimeout(3 * 1000);    // 커넥션 타임아웃 3초
        this.rest = new RestTemplate(factory);
        this.rest.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        this.rest.getMessageConverters().add(new FormHttpMessageConverter());
        Log.d(TAG, "initRestTemplate : 22");
    }

    private class InitRestTemplateAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... deviceRequests) {
            Log.d(TAG, "InitRestTemplateAsyncTask : doInBackground");
            // RestTemplate 초기화 될 때 UI 스레드에 약 600ms 정도 영향을 주는 문제가 발생하여 초기화 코드 전체를 백그라운드로 처리함.
            initRestTemplate();
            return null;
        }
    }

    private class RegistDeviceAsyncTask extends AsyncTask<DeviceData.DeviceRequest, Void, DeviceData.DeviceResponse> {
        @Override
        protected DeviceData.DeviceResponse doInBackground(DeviceData.DeviceRequest... deviceRequests) {
            try {
                DeviceData.DeviceResponse data = rest.postForObject(URI.create(setting.getHost() + "/api/device/register"), deviceRequests[0], DeviceData.DeviceResponse.class);
                if (data.getData() != null && data.getData().getEnableWifi() != null) {
                    setting.setEnableWifi(data.getData().getEnableWifi());
                }
                Log.d(TAG, "Success 7 : " + data.toString());
                return data;
            } catch (Exception e) {
                Log.d(TAG, "Exception 7: "+e.getClass().getName());
                return null;
            }
        }

        @Override
        protected void onPostExecute(DeviceData.DeviceResponse deviceResponse) {
            super.onPostExecute(deviceResponse);
            // 응답이 있을 경우, deviceUuid 를 저장해서 디바이스 등록 중복을 방지함
            if (deviceResponse == null) {
                return;
            }
            devicePrefs.setDeviceUuid(deviceResponse.getData().getDeviceUuid().toString());
        }
    }

    private class VisitRequestAsyncTask extends AsyncTask<VisitDataV2.VisitRequest, Void, VisitDataV2.VisitResponse> {
        @Override
        protected VisitDataV2.VisitResponse doInBackground(VisitDataV2.VisitRequest... visitRequests) {
            try {
                VisitDataV2.VisitResponse response = rest.postForObject(URI.create(setting.getHost() + "/api/v2/visit/checkin"), visitRequests[0], VisitDataV2.VisitResponse.class);

                if ( response == null )
                    return null;

                if (response.getData() != null && response.getData().getEnableWifi() != null)
                    Setting.getInstance().setEnableWifi(response.getData().getEnableWifi());

                String json = new ObjectMapper().writeValueAsString(response);
                devicePrefs.setLastResult(json);
                Log.d(TAG, "visit success: " + json);
                return response;
            } catch (Exception e) {
                Log.d(TAG, "Exception 8: "+e.getClass().getName());
                return null;
            }
        }
    }

    private class AdidRequestAsyncTask extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            AdvertisingIdClient.Info idInfo = null;
            try {
                idInfo = AdvertisingIdClient.getAdvertisingIdInfo(appContext);
                Log.d("com.zeroweb.checkin", "eunho 1 AdidRequestAsyncTask");
                if (DevicePrefs.getInstance(appContext).getAdid().equals("")) {
                    idInfo = AdvertisingIdClient.getAdvertisingIdInfo(appContext);
                }

            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
                Log.d("com.zeroweb.checkin", "exception : " + e.getClass().getName());
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
                Log.d("com.zeroweb.checkin", "exception : " + e.getClass().getName());
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("com.zeroweb.checkin", "exception : " + e.getClass().getName());
            }
            String id = "";
            if (idInfo == null) {
                id = DevicePrefs.getInstance(appContext).getAdid();
            } else {
                id = idInfo.getId();
                DevicePrefs.getInstance(appContext).setAdId(id);
            }
            Log.d("com.zeroweb.checkin", "adid : " + id);
            return id;
        }

        @Override
        protected void onPostExecute(String adId) {
            super.onPostExecute(adId);
            req.setAdid(adId);
            //            request 정보 출력
            Log.d(TAG, "## [REG_DEVICE_PACKET] ## : " + req.toString());

            // ADID 랑 identifier(device_id) 둘다 없는 경우, 모바일 장치 해시값을 저장
            if ( req.getAdid() == null && req.getIdentifier() == null ) {
                EncryptionSHA512 encryption_sha = new EncryptionSHA512();
                String sha_id = encryption_sha.encrypt(Build.SERIAL);
                req.setShaId(sha_id);
            }
            Log.d(TAG, "eunho 1 : " + req.getAdid());
            Log.d(TAG, "API Key is : " + apiKey);
            // registDevice 요청을 보냄
            new RegistDeviceAsyncTask().execute(req);
        }
    }
    DeviceData.DeviceRequest req;

    private void registDevice() {
        if (instance == null)
            return;

        // deviceId 를 기록한다면, deviceId 값을 가져온다
        String device_id = "";
        if ( this.isRegistDeviceId )
            device_id = Build.SERIAL;

        req = new DeviceData.DeviceRequest();

        // deviceUuid 값을 request 에 포함
        if (devicePrefs.getDeviceUuid() != null && !devicePrefs.getDeviceUuid().equals(""))
            req.setDeviceUuid(UUID.fromString(devicePrefs.getDeviceUuid()));

        req.setApiKey(apiKey);
        req.setModel(Build.MODEL);
        req.setIdentifier(device_id);
        req.setAdid("");
        req.setOsType(OsType.ANDROID);
        req.setOsVersion(String.valueOf(Build.VERSION.RELEASE));
        Log.d(TAG, "eunho 3 registDevice");

        try {
            if (this.isRegistADID && GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(appContext) == ConnectionResult.SUCCESS) {
                new AdidRequestAsyncTask().execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "exception2 : " + e.getClass().getName());
        }
    }

    private void visitRequest(List<WifiData> wData) {
        if (instance == null)
            return;

        VisitDataV2.VisitRequest req = new VisitDataV2.VisitRequest();
        req.setDeviceUuid(UUID.fromString(devicePrefs.getDeviceUuid()));
        req.setAdId("");
        req.setWlanData(wData);
        req.setWlanScanType("FIVE_MEDIAN");
        new VisitRequestAsyncTask().execute(req);
    }

    public void traceLocation() {
        if (instance == null)
            return;
        if ("".equals(DevicePrefs.getInstance(appContext).getApiKey())) {
            return;
        }
        Log.d(TAG, "eunho 1 준비중");
        String deviceUuid = devicePrefs.getDeviceUuid();
        if (deviceUuid == null || "".equals(deviceUuid)) {
            Log.d(TAG, "eunho 2 registDevice");
            registDevice();
        } else {
            try {
                Log.d(TAG, "current scan");
                // 현위치에 대해 스캔
                Wifi.getInstance(appContext, new WifiListener() {
                    @Override
                    public void scanComplete(List<WifiData> list) {
                        Log.d(TAG, list.toString());
                    }

                    @Override
                    public void scanCompleteAvg(List<WifiData> list) {
                        Log.d(TAG, "scanCompleteAvg");
                        // 현위치의 이동을 확인
                        if (lastWifiSignal != null) {
                            double similar = Utils.wifiSimilarity(lastWifiSignal, list);

                            if (similar >= 0.8) {
                                Log.d(TAG, "NO MOVE PROCESS SKIP (Size " + list.size() + " Similar " + similar + ")");
                                SpotService.setIsRun(false);
                                return;
                            }
                        }

                        lastWifiSignal = new ArrayList<WifiData>(list);

                        // 데이터 전송
                        visitRequest(lastWifiSignal);
                    }

                    @Override
                    public void detectAp(WifiData data) { }
                }).startScan();
                // 서버로 위치정보 전송
            } catch (Exception e) {
                // e.printStackTrace();
                Log.d(TAG, "Exception 6 : "+e.getClass().getName());
            }
        }
    }

    public synchronized void locateOnce(Context context, final LocateResultReceiver locateResultReceiver) {

        DevicePrefs prefs = DevicePrefs.getInstance(context);

        if (prefs.getDeviceUuid() == null || "".equals(prefs.getDeviceUuid()) || "-1".equals(prefs.getDeviceUuid())) {
            locateResultReceiver.onFailure();
            return;
        }
        final String deviceUuid = prefs.getDeviceUuid();
        IntentFilter filter = new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                context.unregisterReceiver(this);
                if (!mWifiScanningInWait) {
                    return;
                }
                mWifiScanningInWait = false;
                ArrayList<WifiData> wifiList = new ArrayList<>();
                for (ScanResult result : mWifiManager.getScanResults()) {
                    WifiData wifiItem = new WifiData();
                    wifiItem.setSsid(result.SSID);
                    wifiItem.setBssid(result.BSSID);
                    wifiItem.setCapabilities(result.capabilities);
                    wifiItem.setLevel(result.level);
                    wifiItem.setFrequency(result.frequency);
                    wifiList.add(wifiItem);
                }

                VisitData.VisitRequest req = new VisitData.VisitRequest();
                req.setWlanScanType("ONCE");
                req.setDeviceUuid(UUID.fromString(deviceUuid)); // UUID.fromString(deviceUuid));
                req.setWlanDatas(wifiList);

                LocateRequestTask reqTask = new LocateRequestTask();

                reqTask.setLocateResultReceiver(locateResultReceiver);
                reqTask.execute(req);

            }
        }, filter);

        mWifiScanningInWait = true;
        if (mWifiManager == null) {
            mWifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        }

        mWifiManager.startScan();
    }

    private class LocateRequestTask extends AsyncTask<VisitData.VisitRequest, Void, LocateData> {

        private LocateResultReceiver locateResultReceiver;

        void setLocateResultReceiver(LocateResultReceiver locateResultReceiver) {
            this.locateResultReceiver = locateResultReceiver;
        }

        @Override
        protected LocateData doInBackground(VisitData.VisitRequest... visitRequests) {
            try {
                return locateRequest(visitRequests[0]);
            } catch (Exception e) {
                e.getClass().getName();
                return null;
            }
        }

        protected void onPostExecute(LocateData response) {
            if (response == null) {
                locateResultReceiver.onFailure();
                return;
            }
            Location location = new Location("Realstep");
            location.setLatitude(response.getData().getLatitude());
            location.setLongitude(response.getData().getLongitude());
            locateResultReceiver.onSuccess(location);
        }
    }

    interface TryOnce<T> {
        T tryOnce();
    }

    private static final int MAX_RETRY = 3;

    private <T> T retry(TryOnce<T> r) {
        int retry = MAX_RETRY;
        while (retry > 0) {
            try {
                return r.tryOnce();
            } catch (RuntimeException e) {
                if (e instanceof HttpClientErrorException) {
                    HttpClientErrorException he = (HttpClientErrorException) e;
                    switch (he.getStatusCode()) {
                        case NOT_ACCEPTABLE:
                            throw new AcceptableException();
                        case UPGRADE_REQUIRED:
                            throw new RequiresUpgradeException();
                        case FORBIDDEN:
                            throw new SystemMaintenanceModeException();
                        default:
                            throw e;
                    }
                } else if (e.getCause() != null && e.getCause() instanceof UnknownHostException) {
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e1) {
                    }
                    retry--;
                    if (retry <= 0) throw e;
                } else if (e.getCause() != null && e.getCause() instanceof EOFException) {
                    retry--;
                    if (retry <= 0) throw e;
                } else {
                    throw e;
                }
            }
        }
        throw new IllegalStateException();
    }

    @Api
    private LocateData locateRequest(final VisitData.VisitRequest req) {
        try {
            return retry(new TryOnce<LocateData>() {
                public LocateData tryOnce(){
                    HttpHeaders requestHeaders = new HttpHeaders();
                    requestHeaders.setContentType(new MediaType("application","json"));
                    HttpEntity<VisitData.VisitRequest> reqEntity = new HttpEntity<VisitData.VisitRequest>(req, requestHeaders);
                    return rest.postForObject(URI.create(setting.getHost() +  "/api/visit/locate"), req, LocateData.class);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}