package com.zeroweb.device.sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Created by Lee on 2016-07-19.
 */
public class GyroSensor implements SensorEventListener {
    private SensorManager mSensorMgr;
    private Context mContext;
    private long mLastTime;
    private long mLastMoveForce;
    private long mLastHoldForce;
    private long mLastHold;
    private long mLastMove;
    private int mHoldCount = 0;
    private int mMoveCount = 0;
    private static final int FORCE_THRESHOLD = 10;
    private static final int TIME_THRESHOLD = 100;
    private static final int HOLD_TIMEOUT = 500;
    private OnGyroListener mGyroListener;
    private float mLastX = -1.0f, mLastY = -1.0f, mLastZ = -1.0f;
    private static final int HOLD_COUNT = 15;
    private static final int MOVE_COUNT = 3;
    private static final int HOLD_DURATION = 1000;
    private static GyroSensor mInstance;

    public interface OnGyroListener {
        /**
         * 센서에 움직임이 감지되는 상태
         */
        void onGyroMove();

        /**
         * 센서에 움직임이 감지되지 않는 상태
         */
        void onGyroHold();
    }

    public GyroSensor(Context context) {
        mContext = context;
    }

    public GyroSensor(Context context, OnGyroListener listener) {
        mContext = context;
        mGyroListener = listener;
    }

    public static GyroSensor getInstance(Context context, OnGyroListener listener) {
        if(mInstance == null)
            mInstance = new GyroSensor(context, listener);
        return mInstance;
    }

    public void setOnShakeListener(OnGyroListener listener) {
        mGyroListener = listener;
    }

    public void startSensor() {
        mSensorMgr = (SensorManager) mContext
                .getSystemService(Context.SENSOR_SERVICE);
        if (mSensorMgr == null) {
            throw new UnsupportedOperationException("Sensors not supported");
        }
        boolean supported = false;
        try {
            supported = mSensorMgr.registerListener(this,
                    mSensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                    SensorManager.SENSOR_DELAY_FASTEST);
        } catch (Exception e) {
            // 자이로센서를 지원하지 않는 경우
        }

        if ((!supported) && (mSensorMgr != null))
            mSensorMgr.unregisterListener(this);
    }

    public void stopSensor() {
        if (mSensorMgr != null) {
            mSensorMgr.unregisterListener(this);
            mSensorMgr = null;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER)
            return;
        long now = System.currentTimeMillis();

        if ((now - mLastMoveForce) > HOLD_TIMEOUT) {
            mMoveCount = 0;
        }
        if ((now - mLastHoldForce) > HOLD_TIMEOUT) {
            mHoldCount = 0;
        }

        if ((now - mLastTime) > TIME_THRESHOLD) {                   // 0.1 초마다 센서의 값을 체크
            long diff = now - mLastTime;
            float speed = Math.abs(event.values[SensorManager.DATA_X]
                    + event.values[SensorManager.DATA_Y]
                    + event.values[SensorManager.DATA_Z] - mLastX - mLastY
                    - mLastZ)
                    / diff * 10000;
            if (speed > FORCE_THRESHOLD) {                          // 속도가 FORCE_THRESHOLD 보다 클때
                if ((++mMoveCount >= MOVE_COUNT)                    // 이 Case 가 5번이상 지속되고
                        && (now - mLastMove > HOLD_DURATION)) {     // 마지막 감지시간이 HOLD_DURATION 보다 크면 콜백 (1초이상 속도가 느껴지면 콜백)
                    mLastMove = now;
                    mMoveCount = 0;

                    if (mGyroListener != null) {
                        mGyroListener.onGyroMove();
                    }
                }
                mLastMoveForce = now;
            } else {
                if ((++mHoldCount >= HOLD_COUNT)                    // 속도가 FORCE_THRESHOLD 보다 작을때 이 Case 가 15번이상 지속되고
                        && (now - mLastHold > HOLD_DURATION)) {     // 마지막 감지시간이 HOLD_DURATION 보다 크면 콜백 (1초이상 속도가 느껴지면 콜백)
                    mLastHold = now;
                    mHoldCount = 0;

                    if (mGyroListener != null) {
                        mGyroListener.onGyroHold();
                    }
                }
                mLastHoldForce = now;
            }
            mLastTime = now;
            mLastX = event.values[SensorManager.DATA_X];
            mLastY = event.values[SensorManager.DATA_Y];
            mLastZ = event.values[SensorManager.DATA_Z];

//            Log.d(this.getClass().getName(), now + " X: " + mLastX + "/ Y:" + mLastY + "/ Z:" + mLastZ + "/ SPEED:" + speed);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
