package com.zeroweb.device.sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Created by Lee on 2016-07-19.
 */
public class BrightSensor implements SensorEventListener {
    private SensorManager mSensorMgr;
    private Context mContext;
    private OnBrightListener mBrightSensorListener;
    private static BrightSensor mInstance;

    public interface OnBrightListener {
        void onBrightOff();
        void onBrightOn();
    }

    public BrightSensor(Context context) {
        mContext = context;
    }

    public BrightSensor(Context context, OnBrightListener listener) {
        mContext = context;
        mBrightSensorListener =  listener;
    }

    public void setOnBrightListener(OnBrightListener listener) {
        mBrightSensorListener = listener;
    }

    public void startSensor() {
        mSensorMgr = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        if (mSensorMgr == null) {
            throw new UnsupportedOperationException("Sensors not supported");
        }
        boolean supported = false;
        try {
            supported = mSensorMgr.registerListener(this,
                    mSensorMgr.getDefaultSensor(Sensor.TYPE_LIGHT),
                    SensorManager.SENSOR_DELAY_FASTEST);
        } catch (Exception e) {
            // 조도센서를 지원하지 않는 경우
        }

        if ((!supported) && (mSensorMgr != null))
            mSensorMgr.unregisterListener(this);
    }

    public void stopSensor() {
        if (mSensorMgr != null) {
            mSensorMgr.unregisterListener(this);
            mSensorMgr = null;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() != Sensor.TYPE_LIGHT)
            return;
        long now = System.currentTimeMillis();

//        Log.d(this.getClass().getName(), now + " Brightness Sensor Value : " + event.values[0]);        // 조도값 event.values[0]
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
