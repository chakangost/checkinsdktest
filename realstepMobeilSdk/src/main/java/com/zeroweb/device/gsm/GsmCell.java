package com.zeroweb.device.gsm;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;

/**
 * Created by Lee on 2016-08-04.
 */
public class GsmCell {
    private Context mContext;
    private int mCell;
    private int mLac;
    private int mPsc;
    private PhoneStateListener mListener;
    private static GsmCell mInstance;

    public GsmCell(Context context) {
        mContext = context;
        getData();
    }

    private GsmCell(Context context, PhoneStateListener listener) {
        mContext = context;
        mListener = listener;
        getData();
    }

    /**
     * 객체의 instance를 반환
     * @param context
     * @param listener
     * @return
     */
    public static GsmCell getInstance(Context context, PhoneStateListener listener) {
        if(mInstance == null)
            mInstance = new GsmCell(context, listener);
        return mInstance;
    }

    private void getData() {
        TelephonyManager tm = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);

        GsmCellLocation loc = (GsmCellLocation)tm.getCellLocation();

        mCell = loc.getCid();
        mLac = loc.getLac();
        mPsc = loc.getPsc();

        if (mListener != null)
            tm.listen(mListener, PhoneStateListener.LISTEN_CELL_LOCATION);
    }

    public void stopListener() {
        TelephonyManager tm = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
        tm.listen(mListener, PhoneStateListener.LISTEN_NONE);
    }

    public int getCell() {
        return mCell;
    }

    public int getLac() {
        return mLac;
    }

    public int getPsc() {
        return mPsc;
    }
}
