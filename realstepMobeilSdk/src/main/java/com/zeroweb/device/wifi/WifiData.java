package com.zeroweb.device.wifi;

import java.net.URLEncoder;

/**
 * Created by Lee on 2016-08-04.
 */
public class WifiData {
    private String bssid;
    private String ssid;
    private String capabilities;
    private int frequency;
    private int level;
    private int dupeCount = 0;

    public int getDupeCount() {
        return dupeCount;
    }

    public void setDupeCount(int dupeCount) {
        this.dupeCount = dupeCount;
    }

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(String capabilities) {
        this.capabilities = "";
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getParams(String id) throws Exception{
        return "spotid=" + id
                + "&bssid=" + URLEncoder.encode(getBssid(), "UTF-8")
                + "&ssid=" + URLEncoder.encode(getSsid(), "UTF-8")
                + "&capabilities=" + getCapabilities()
                + "&frequency=" + getFrequency()
                + "&level=" + getLevel();
    }
}
