package com.zeroweb.device.wifi;

import java.util.Comparator;

/**
 * Created by Lee on 2016-09-28.
 */

public class LevelComparator implements Comparator<WifiData> {

    @Override
    public int compare(WifiData f, WifiData s) {
        if (f.getLevel() > s.getLevel())
            return -1;
        else if (f.getLevel() < s.getLevel())
            return 1;
        else
            return 0;
    }
}
