package com.zeroweb.device.wifi;

import java.util.List;

/**
 * Created by Lee on 2016-08-04.
 */
public interface WifiListener {
    /**
     * Scan 의 동작이 완료되는 시점에 호출
     * @param list
     */
    void scanComplete(List<WifiData> list);

    /**
     * Scan 의 동작이 완료되는 시점에 호출
     * @param list
     */
    void scanCompleteAvg(List<WifiData> list);

    /**
     * Scan 도중 ap가 검색될때마다 호출
     * @param data
     */
    void detectAp(WifiData data);
}
