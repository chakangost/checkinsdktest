package com.zeroweb.device.wifi;

import android.Manifest;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Process;
import android.util.Log;

import com.zeroweb.rsi.Setting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Lee on 2016-08-04.
 */
public class Wifi {
    private Context mContext;
    private static Wifi mInstance;
    private WifiManager mWifiManager;
    private WifiListener mListener;
    private Handler mHandler;
    private List<WifiData> mDetectList = new ArrayList<>();
    private List<WifiData> mDetectAvgList = new ArrayList<>();
    private boolean mWifiStatusFlag = true;
    private int mLoopCount = 0;
    private Class<WifiManager> mWifiManagerClass;
    CountDownTimer mCountDown = null;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                final String action = intent.getAction();
                boolean isScanWifi = false;

                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                    if (context.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, android.os.Process.myPid(), Process.myUid()) == PackageManager.PERMISSION_GRANTED &&
                            context.checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION, android.os.Process.myPid(), Process.myUid()) == PackageManager.PERMISSION_GRANTED) {
                        isScanWifi = true;
                    }
                } else {
                    isScanWifi = true;
                }

                if (action.equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                    try {
                        stopScan();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (action.equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION) && isScanWifi) {

                    mCountDown = new CountDownTimer(1500*mLoopCount, 1500) {
                        @Override
                        public void onTick(long l) {
                            List<ScanResult> results = (mWifiManager.getScanResults() == null) ? new ArrayList<ScanResult>() : new ArrayList<ScanResult>(mWifiManager.getScanResults());
                            for (ScanResult result : results) {
                                if (result.SSID.contains("AndroidHotspot") || result.SSID.contains("iPhone"))
                                    continue;
                                WifiData ap = new WifiData();
                                ap.setBssid(result.BSSID);
                                ap.setCapabilities(result.capabilities);
                                ap.setFrequency(result.frequency);
                                ap.setLevel(result.level);
                                ap.setSsid(result.SSID);

                                mDetectList.add(ap);
                                mListener.detectAp(ap);
                            }

                            mLoopCount--;
                            Log.d("com.zeroweb.checkin", "mLoopCount is : " + mLoopCount);
                            Log.d("com.zeroweb.checkin", " mDetectList size  : " + mDetectList.size());
                        }

                        @Override
                        public void onFinish() {
                            Map<String, List<WifiData>> bssid_to_datas = new HashMap<String, List<WifiData>>();
                            List<WifiData> copyDetectList = (mDetectList == null) ? new ArrayList<WifiData>() : new ArrayList<>(mDetectList);

                            for (WifiData wd : copyDetectList) {
                                if ( wd.getBssid() == null )
                                    continue;
                                List<WifiData> l = bssid_to_datas.get(wd.getBssid());
                                if (l == null)
                                    l = new ArrayList<WifiData>();
                                l.add(wd);
                                bssid_to_datas.put(wd.getBssid(), l);
                            }
                            for (String bssid : bssid_to_datas.keySet()) {
                                List<WifiData> wifidatas = bssid_to_datas.get(bssid);

                                // 중앙값
                                Collections.sort(wifidatas, new LevelComparator());
                                WifiData data = wifidatas.get(wifidatas.size() / 2);
                                data.setDupeCount(3);
                                mDetectAvgList.add(data);
                            }

//                            if (mListener != null)
//                                mListener.scanComplete(copyDetectList);
                            if (mListener != null)
                                mListener.scanCompleteAvg(mDetectAvgList);
                        }
                    }.start();
                } else if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                    mContext.sendBroadcast(new Intent("wifi.ON_NETWORK_STATE_CHANGED"));
                }
            } catch (Exception e) {
                Log.d("Exception22", "wifi receiver" + e.toString());
                e.printStackTrace();
            }
        }
    };

    public Wifi(Context context) {
        mContext = context;
        initWifiScan();
    }

    private Wifi(Context context, WifiListener listener) {
        mContext = context;
        mListener = listener;
        initWifiScan();
    }

    private void initWifiScan() {
        mWifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        mWifiManagerClass = (Class<WifiManager>) mWifiManager.getClass();
    }

    private boolean isScanEnabled() {
        if (mWifiManager.getWifiState() != WifiManager.WIFI_STATE_ENABLED
                && mWifiManager.getWifiState() != WifiManager.WIFI_STATE_ENABLING) {
            if (Build.VERSION.SDK_INT >= 18) {
                if (!mWifiManager.isScanAlwaysAvailable()) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * 스캔을 시작
     * @param loopCount 스캔 반복 횟수 (Avg의 표준편차를 줄이기 위함)
     */
    public void startScan(final int loopCount) {
        // Wifi가 활성화 상태가 아니라면 강제 활성화
        if ( Setting.getInstance().isEnableWifi() && !isScanEnabled()) {
            mWifiManager.setWifiEnabled(true);
            mWifiStatusFlag = false;
        }

        mLoopCount = loopCount;
        mDetectList.clear();
        mDetectAvgList.clear();

        final IntentFilter filter = new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        mContext.registerReceiver(mReceiver, filter);
        mWifiManager.startScan();
    }

    public void startScan() {
        startScan(5);
    }

    public void stopScan() {
        // 강제 활성화된 Wifi라면 다시 종료
        if ( Setting.getInstance().isEnableWifi() && !isScanEnabled() && !mWifiStatusFlag) {
            mWifiManager.setWifiEnabled(false);
            mWifiStatusFlag = true;
        }
        mContext.unregisterReceiver(mReceiver);
        mHandler = null;
    }

    /**
     * 리스너 변경
     * @param listener
     */
    public void setListener(WifiListener listener) {
        mListener = listener;
    }

    /**
     * 객체의 instance를 반환
     * @param context
     * @param listener
     * @return
     */
    public static Wifi getInstance(Context context, WifiListener listener) {
        if (mInstance == null)
            mInstance = new Wifi(context, listener);
        return mInstance;
    }
}
