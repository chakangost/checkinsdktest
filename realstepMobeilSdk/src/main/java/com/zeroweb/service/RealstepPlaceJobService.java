package com.zeroweb.service;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.zeroweb.spot.RealstepSpot;
import com.zeroweb.util.Utility;

@RequiresApi(api = Build.VERSION_CODES.M)
public class RealstepPlaceJobService extends JobService {
    private Utility utility = Utility.getInstance();

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        RealstepSpot.getInstance(getApplicationContext()).traceLocation();
        Log.d("com.zeroweb.checkin", "RealstepPlaceJobService");
        utility.scheduleJob(getApplicationContext());
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return true;
    }
}
