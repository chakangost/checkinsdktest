package com.example.chakangost.checkinsdktest;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.zeroweb.rsi.Realstep;

public class TestApplication extends MultiDexApplication {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

        Realstep.init(this);
        Realstep.setLoopTime(60 * 1000);
    }
}