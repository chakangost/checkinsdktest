package com.example.chakangost.checkinsdktest;

import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.zeroweb.rsi.Realstep;
import com.zeroweb.rsi.vo.LocateResultReceiver;

public class MainActivity extends AppCompatActivity {

    TimerHandler timerHandler = null;
    private static final int MESSAGE_TIMER_START = 100;
    private static final int MESSAGE_TIMER_REPEAT = 101;
    private static final int MESSAGE_TIMER_STOP = 102;
    Button buttonStart, buttonStop, buttonRepeat, buttonPlus;
    TextView textView;
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timerHandler = new TimerHandler();

        buttonStart = findViewById(R.id.start);
        textView = findViewById(R.id.textView);
        buttonRepeat = findViewById(R.id.repeat);
        buttonStop = findViewById(R.id.stop);
        buttonPlus = findViewById(R.id.button2);

        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timerHandler.sendEmptyMessage(MESSAGE_TIMER_START);
            }
        });
        buttonRepeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timerHandler.sendEmptyMessage(MESSAGE_TIMER_REPEAT);
            }
        });
        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timerHandler.sendEmptyMessage(MESSAGE_TIMER_STOP);
            }
        });
        buttonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText("Timer Repeat : " + count++);

                Realstep.requestLocate(MainActivity.this, new LocateResultReceiver() {
                    @Override
                    public void onSuccess(Location location) {
                        super.onSuccess(location);
                        if (location != null) {
                            Toast.makeText(MainActivity.this, "위도 : " + location.getLatitude() + "\n" +
                                    "경도 : " + location.getLongitude(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure() {
                        super.onFailure();
                        Toast.makeText(MainActivity.this, "위치가져오기 실패", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private class TimerHandler extends Handler {
        int tempCount = 0;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_TIMER_START:
                    count = 0;
                    Log.d("TimerHandler", "Timer start.");
                    this.removeMessages(MESSAGE_TIMER_REPEAT);
                    this.sendEmptyMessage(MESSAGE_TIMER_REPEAT);
                    break;
                case MESSAGE_TIMER_REPEAT:
                    tempCount = count++;
                    Log.d("TimerHandler", "Timer Repeat : " + tempCount);
                    textView.setText("Timer Repeat : " + tempCount);
                    this.sendEmptyMessageDelayed(MESSAGE_TIMER_REPEAT, 1000);
                    break;
                case MESSAGE_TIMER_STOP:
                    Log.d("TimerHandler", "Timer End.");
                    this.removeMessages(MESSAGE_TIMER_REPEAT);
                    count = 0;
                    break;
            }
        }
    }
}